﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(StateMachine))]
public class PlayerBrawler : MonoBehaviour {
    public float verticalSpeed, movementSpeed;
    public GameObject weapon;
    private Attack punch;
    bool facingRight = false;
    private Animator animator;
    private StateMachine stateMachine;
    

    public KeyCode attackButton = KeyCode.Space;

    void Start () {
        animator = GetComponentInChildren<Animator>();
        punch = GetComponent<Attack>();
        stateMachine = GetComponent<StateMachine>();
	}


    void Update()
    {

        Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        //input.magnitude --> walk/idle animation transition
        animator.SetFloat("MovementSpeed", input.sqrMagnitude);

        if (stateMachine.currentState != PlayerState.Attacking
            && stateMachine.currentState != PlayerState.Drinking
            && stateMachine.currentState != PlayerState.Hurt
            && stateMachine.currentState != PlayerState.Dead)
        { 
            if ((input.x > 0 && !facingRight) || (input.x < 0 && facingRight))
            {
                Flip();
            }

            Vector2 newPos = new Vector3(
                        Mathf.Clamp(transform.position.x + input.x * movementSpeed * Time.deltaTime, GameVariables.Instance.minX, GameVariables.Instance.maxX),
                        Mathf.Clamp(transform.position.y + input.y * verticalSpeed * Time.deltaTime, GameVariables.Instance.minY, GameVariables.Instance.maxY),
                        transform.position.z
                    );

            transform.position = newPos;


            if (Input.GetKeyDown(attackButton))
            {
                Attack();
            }
        }
    }
    void Flip()
    {
        facingRight = !facingRight;

        Vector3 newScale = transform.localScale;
        newScale.x *= -1;
        transform.localScale = newScale;
    }

    void Attack()
    {
        if (weapon != null)
        {
            Debug.Log("Bottle Smash");
            animator.SetTrigger("BottleHit");

        } else
        {
            animator.SetTrigger("Punch");
            punch.StartAttack();
        }
    }
}
