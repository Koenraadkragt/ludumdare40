﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuDrunk : MonoBehaviour {

    public Image sprite;
    public Sprite one;
    public Sprite two;
    private bool isOne = false;

	// Use this for initialization
	void Start () {
        StartCoroutine("DrinkSprite");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator DrinkSprite()
    {
        yield return new WaitForSeconds(1.0f);
        if (isOne)
        {
            sprite.sprite = one;
        }
        else
        {
            sprite.sprite = two;
        }
        isOne = !isOne;
        StartCoroutine("DrinkSprite");
    }
}
