﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseBehaviour : MonoBehaviour {

    public GameObject target;
    public float movementSpeed, verticalSpeed;
    public float attackRange;
    bool facingRight = false;
    private StateMachine state;
    private StateMachine targetState;

    Animator animator;

	// Use this for initialization
	void Start () {
		if (target ==null)
        {
            target = GameObject.FindGameObjectWithTag("Player");
        }
        animator = GetComponentInChildren<Animator>();
        state = GetComponent<StateMachine>();
        targetState = target.GetComponent<StateMachine>();
	}
	
	// Update is called once per frame
	void Update () {

        if (state.currentState != PlayerState.Attacking && state.currentState != PlayerState.Hurt && targetState.currentState != PlayerState.Dead)
        {
            if ((target.transform.position - transform.position).sqrMagnitude < attackRange*attackRange)
            {
                //attack
                animator.SetTrigger("Hit");
                GetComponent<Attack>().StartAttack();
            }
            else
            {
                Move();

            }

        }

        transform.position = new Vector3(
                    Mathf.Clamp(transform.position.x, GameVariables.Instance.minX, GameVariables.Instance.maxX),
                    Mathf.Clamp(transform.position.y, GameVariables.Instance.minY, GameVariables.Instance.maxY),
                    transform.position.z
                );

    }

    void Move ()
    {
        Vector3 targetDirection = target.transform.position - transform.position;
        targetDirection.x = Mathf.Max(-movementSpeed, Mathf.Min(targetDirection.x, movementSpeed));
        targetDirection.y = Mathf.Max(-verticalSpeed, Mathf.Min(targetDirection.y, verticalSpeed));

        //targetDirection.magnitude --> walk/idle animation
        animator.SetFloat("speed", targetDirection.magnitude);

        if ((targetDirection.x > 0 && !facingRight) || (targetDirection.x < 0 && facingRight))
        {
            Flip();
        }

        transform.position = new Vector3(
                    transform.position.x + targetDirection.x * Time.deltaTime,
                    transform.position.y + targetDirection.y * Time.deltaTime,
                    transform.position.z
                );
    }

    void Flip ()
    {
        facingRight = !facingRight;

        Vector3 newScale = transform.localScale;
        newScale.x *= -1;
        transform.localScale = newScale;
    }
    
}
