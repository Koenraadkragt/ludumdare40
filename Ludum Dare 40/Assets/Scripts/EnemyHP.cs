﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHP : MonoBehaviour {

    public int health;

    public void TakeDamage(int damage)
    {
        if (health - damage <= 0)
        {
            StartCoroutine("Death");
        }
        else
        {
            health -= damage;
        }
        GetComponentInChildren<Animator>().SetBool("Hurt", true);
        GetComponent<StateMachine>().currentState = PlayerState.Hurt;
        Invoke("resetPlayerState", 1.0f);
    }

    void resetPlayerState()
    {
        GetComponentInChildren<Animator>().SetBool("Hurt", false);
        GetComponent<StateMachine>().currentState = PlayerState.Idle;
    }

    IEnumerator Death()
    {
        yield return new WaitForSeconds(0.5f);
        GameObject.Find("GameManager").GetComponent<ScoreManager>().AddScore(100);
        Destroy(this.gameObject);
    }

}
