﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaskManager : MonoBehaviour {

    private Image mask;
    private Image healthbar;

    public void Start()
    {
        mask = GameObject.Find("HPImage").GetComponent<Image>();
        healthbar = GameObject.Find("MaskImage").GetComponent<Image>();
        ChangeHealth(100);
    }

    public void ToggleMask()
    {
        mask.enabled = !mask.enabled;
    }

    public void ChangeHealth(float hp)
    {
        healthbar.rectTransform.sizeDelta = new Vector2(280 * (hp/100), 100);
    }

}
