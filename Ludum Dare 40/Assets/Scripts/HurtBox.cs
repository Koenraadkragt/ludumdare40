﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtBox : MonoBehaviour {

    StateMachine state;
	void Start () {
        state = GetComponent<StateMachine>();		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "HitBox" && state.currentState != PlayerState.Hurt)
        {            
            if(this.gameObject.tag == "Player")
            {
                GetComponent<ImmortalityFrame>().TriggerFlash();
                this.gameObject.GetComponent<PlayerStats>().TakeDamage(10);
                if (collision.gameObject.GetComponent<BottleThrow>() != null)
                {
                    Destroy(collision.gameObject);
                    GameObject.Find("BottleSound").GetComponent<AudioSource>().Play();
                }
            } else if(this.gameObject.tag =="Enemy" && collision.gameObject.GetComponentInParent<PlayerStats>() != null)
            {
                GetComponent<ImmortalityFrame>().TriggerFlash();
                this.gameObject.GetComponent<EnemyHP>().TakeDamage(GameObject.Find("Player").GetComponent<PlayerStats>().baseDamage);
                GameObject.Find("PunchSound").GetComponent<AudioSource>().Play();
            }
        }
    }
}
