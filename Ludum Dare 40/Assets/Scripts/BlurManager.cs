﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlurManager : MonoBehaviour {

    Material shaderMaterial;
    public float blur = 0;

    void Start()
    {
        shaderMaterial = GameObject.Find("Main Camera").GetComponent<CustomImageEffect>().EffectMaterial;
        UpdateShader();
    }

    // Update is called once per frame
    void UpdateShader () {
        shaderMaterial.SetFloat("_Magnitude", blur);
	}

    public void IncreaseDrunk(float value)
    {
        blur += value;
        UpdateShader();
    }

    private void OnApplicationQuit()
    {
        blur = 0;
        UpdateShader();
    }
}
