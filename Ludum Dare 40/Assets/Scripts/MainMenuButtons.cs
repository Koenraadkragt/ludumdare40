﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuButtons : MonoBehaviour {

    public Image[] buttons;
    private Sprite[] baseImage;
    public Sprite[] altImage;
    private int selected = 0;
    public AudioSource select;
    public AudioSource other;
    private string sceneToLoad;

	// Use this for initialization
	void Start () {
        baseImage = new Sprite[buttons.Length];
		for(int i = 0; i < buttons.Length; i++)
        {
            baseImage[i] = buttons[i].GetComponent<Image>().sprite;
        }
        buttons[selected].sprite = altImage[selected];
    }
	
	// Update is called once per frame
	void Update () {
        if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (selected == 0)
            {
                selected = buttons.Length - 1;
            }
            else
            {
                selected -= 1;
            }

            for (int i = 0; i < buttons.Length; i++)
            {
                buttons[i].sprite = baseImage[i];
            }
            buttons[selected].sprite = altImage[selected];
            other.Play();
        }

        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (selected == buttons.Length-1)
            {
                selected = 0;
            }
            else
            {
                selected +=1;
            }

            for(int i = 0; i < buttons.Length; i++)
            {
                buttons[i].sprite = baseImage[i];
            }
            buttons[selected].sprite = altImage[selected];
            other.Play();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            select.Play();
            
            switch (selected)
            {
                case 0:
                    if(SceneManager.GetActiveScene().name == "MainMenu")
                    {
                        sceneToLoad = "Testscene - Koenraad 3";
                    }
                    if (SceneManager.GetActiveScene().name == "Credits")
                    {
                        sceneToLoad = "Help";
                    }
                    if (SceneManager.GetActiveScene().name == "Help")
                    {
                        sceneToLoad = "Credits";
                    }
                    if(SceneManager.GetActiveScene().name == "GameOver")
                    {
                        sceneToLoad = "MainMenu";
                    }                    
                    break;
                case 1:
                    if (SceneManager.GetActiveScene().name == "MainMenu")
                    {
                        sceneToLoad = "Credits";
                    }
                    if (SceneManager.GetActiveScene().name == "Credits")
                    {
                        sceneToLoad = "MainMenu";
                    }
                    if (SceneManager.GetActiveScene().name == "Help")
                    {
                        sceneToLoad = "MainMenu";
                    }
                    break;
                case 2:
                    sceneToLoad = "Help";
                    break;
            }
            StartCoroutine("WaitForSound");
        }
    }

    IEnumerator WaitForSound()
    {
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene(sceneToLoad);
    }
}
