﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour {

    public GameObject hitbox;
    public float startupTime, activeTime, recoveryTime;
    private IEnumerator coroutine;

    private void Start()
    {
        coroutine = AttackSequence();
    }
    public void StartAttack()
    {
        GetComponent<StateMachine>().currentState = PlayerState.Attacking;
        StartCoroutine(AttackSequence());
        
    }

    IEnumerator AttackSequence()
    {
        yield return new WaitForSeconds(startupTime);
        hitbox.SetActive( true );
        yield return new WaitForSeconds(activeTime);
        hitbox.SetActive(false);
        yield return new WaitForSeconds(recoveryTime);
        GetComponent<StateMachine>().currentState = PlayerState.Idle;
    }

    public void CancelAttack()
    {
        Debug.Log("cancel attack");
        StopCoroutine(coroutine);
        hitbox.SetActive(false);
        GetComponent<StateMachine>().currentState = PlayerState.Idle;

    }
}
