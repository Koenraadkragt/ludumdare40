﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameVariables : MonoBehaviour {

    #region SINGLETON
    static GameVariables _instance;

    public static GameVariables Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameVariables>();
            }
            return _instance;
        }
    }
    #endregion
    

    public float maxX, minX, maxY, minY;
    public float maxDepth, minDepth;
}
