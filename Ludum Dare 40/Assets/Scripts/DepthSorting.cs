﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepthSorting : MonoBehaviour {

    public SpriteRenderer target;

	void Start () {
		if (target == null)
        {
            target = GetComponent<SpriteRenderer>();
        }
	}
	
	// Update is called once per frame
	void Update () {

        float relativeDepth = 1 - (transform.position.y - GameVariables.Instance.minY) / (GameVariables.Instance.maxY - GameVariables.Instance.minY);
        target.sortingOrder = (int) (relativeDepth * GameVariables.Instance.maxDepth);
    }
}
