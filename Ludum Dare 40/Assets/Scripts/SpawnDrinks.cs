﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnDrinks : MonoBehaviour {

    public float spawnInterval;
    public Drink[] drinks;

    List<Drink> common = new List<Drink>();
    List<Drink> uncommon = new List<Drink>();
    List<Drink> rare = new List<Drink>();
    List<Drink> veryRare = new List<Drink>();
    List<Drink> mythical = new List<Drink>();

    // Use this for initialization
    void Start () {
        foreach(Drink drink in drinks)
        {
            switch (drink.rarity)
            {
                case Drink.Rarity.Common:
                    common.Add(drink);
                    break;

                case Drink.Rarity.Uncommon:
                    uncommon.Add(drink);
                    break;

                case Drink.Rarity.Rare:
                    rare.Add(drink);
                    break;

                case Drink.Rarity.VeryRare:
                    veryRare.Add(drink);
                    break;

                case Drink.Rarity.Mythical:
                    mythical.Add(drink);
                    break;
            }
        }

        StartCoroutine("SpawnInterval",spawnInterval);
    }
	
    void SpawnDrink()
    {
        Drink spawn;
        int i = Random.Range(1, 100);
        if (i <= 1)
        {
            if (!mythical.Count.Equals(0))
            {
                spawn = mythical[Random.Range(0, mythical.Count)];
            }
            else
            {
                spawn = common[Random.Range(0, common.Count)];
            }
        }
        else if(i<=10)
        {
            if (!veryRare.Count.Equals(0))
            {
                spawn = veryRare[Random.Range(0, veryRare.Count)];
            }
            else
            {
                spawn = common[Random.Range(0, common.Count)];
            }
        }
        else if (i <= 25)
        {
            if (!rare.Count.Equals(0))
            {
                spawn = rare[Random.Range(0, rare.Count)];
            }
            else
            {
                spawn = common[Random.Range(0, common.Count)];
            }
        }
        else if (i <= 50)
        {
            if (!uncommon.Count.Equals(0))
            {
                spawn = uncommon[Random.Range(0, uncommon.Count)];
            }
            else
            {
                spawn = common[Random.Range(0, common.Count)];
            }
        }
        else
        {
            spawn = common[Random.Range(0, common.Count)];
        }

        Camera c = Camera.main;
        Vector3 spawnPosition = c.ViewportToWorldPoint(new Vector3(Random.Range(0.15f,0.85f),0,0));

        Drink temp = Instantiate(spawn,spawnPosition,Quaternion.identity);
        temp.transform.position = new Vector3(temp.transform.position.x, 1.35f, 5);
    }

    IEnumerator SpawnInterval(float spawnInterval)
    {
        yield return new WaitForSeconds(spawnInterval);
        SpawnDrink();
        StartCoroutine("SpawnInterval",spawnInterval+0.1f);
    }
}
