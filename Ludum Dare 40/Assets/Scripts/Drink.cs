﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drink : MonoBehaviour {

    public int healValue;
    public float blurValue;
    public int score;

    public enum Rarity
    {
        Common,
        Uncommon,
        Rare,
        VeryRare,
        Mythical
    }

    public Rarity rarity;

    private void Start()
    {
        this.gameObject.name = this.gameObject.name.Substring(0,gameObject.name.Length-7);
    }

    void OnDestroy()
    {
        GameObject.Find("GameManager").GetComponent<ScoreManager>().AddScore(score);
    }
}
