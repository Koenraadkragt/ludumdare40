﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerStats : MonoBehaviour {

    public int baseDamage;
    public int maxHP;
    int damage;
    int hp;
    public MaskManager manager;

	void Start () {
        hp = maxHP;
        damage = baseDamage;
        manager = GameObject.Find("GameManager").GetComponent<MaskManager>();
    }

    public void EquipWeapon(int bonusDamage)
    {
        damage = baseDamage + bonusDamage;
    }

    public void ResetDamage()
    {
        damage = baseDamage;
    }

    public void Heal(int heal)
    {
        if ((hp + heal) >= maxHP)
        {
            hp = maxHP;
        }
        else
        {
            hp += heal;
        }
        manager.SendMessage("ChangeHealth", (float)hp / (float)maxHP * 100);
    }

    public void TakeDamage(int damage)
    {
        hp -= damage;
        manager.SendMessage("ChangeHealth", (float)hp / (float)maxHP * 100);
        if (hp <= 0)
        {
            StartCoroutine(GameOver());
        }
    }
    
    private IEnumerator GameOver ()
    {
        GetComponentInChildren<Animator>().SetTrigger("Death");
        GetComponent<StateMachine>().currentState = PlayerState.Dead;
        yield return new WaitForSeconds(4.0f);
        SceneManager.LoadScene("GameOver");
    }
}
