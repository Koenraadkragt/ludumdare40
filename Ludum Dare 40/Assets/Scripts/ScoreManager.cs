﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    public Text scoreText;
    private int score;

	// Use this for initialization
	void Start () {
        scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
        score = 0;
        AddScore(0);
	}

    public void AddScore(int score)
    {
        this.score += score;
        scoreText.text = this.score + " Points";
        GameObject.Find("ScoreManager").GetComponent<GameOverScore>().score = this.score;
    }
}
