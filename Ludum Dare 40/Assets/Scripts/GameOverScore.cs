﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverScore : MonoBehaviour {

    public int score;

	void Start () {
        DontDestroyOnLoad(this.gameObject);
	}

	void Update () {
		if(SceneManager.GetActiveScene().name == "GameOver")
        {
            GameObject.Find("ScoreText").GetComponent<Text>().text = "Final Score: " + score;
            Destroy(this.gameObject);
        }
	}
}
