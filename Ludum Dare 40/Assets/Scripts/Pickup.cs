﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {

    public char pickupKey;
    private Animator anim;
    private StateMachine state;
    

    private void Start()
    {
        anim = GetComponentInChildren<Animator>();
        state = GetComponent<StateMachine>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (Input.GetKeyDown((KeyCode)pickupKey) && collision.gameObject.GetComponent<Drink>() != null 
            && state.currentState != PlayerState.Drinking && state.currentState != PlayerState.Attacking)
        {
            Destroy(collision.gameObject);
            this.gameObject.GetComponent<PlayerStats>().SendMessage("Heal", collision.GetComponent<Drink>().healValue);
            GameObject.Find("GameManager").GetComponent<BlurManager>().IncreaseDrunk(collision.GetComponent<Drink>().blurValue);
            this.gameObject.GetComponent<PlayerStats>().EquipWeapon(1);
            
            anim.SetTrigger("Drink");
            state.currentState = PlayerState.Drinking;
            GameObject.Find("DrinkSound").GetComponent<AudioSource>().Play();
            StartCoroutine(OnCompleteDrinkAnimation());
        }
    }

    IEnumerator OnCompleteDrinkAnimation()
    {
        yield return new WaitForSeconds(0.417f);
        GameObject.Find("DrinkSound").GetComponent<AudioSource>().Stop();
        state.currentState = PlayerState.Idle;
    }
}
