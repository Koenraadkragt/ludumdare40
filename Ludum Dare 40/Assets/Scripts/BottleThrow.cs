﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottleThrow : MonoBehaviour {

    public bool spawnLeft;
    private float bottleSpeed;
    public float damage;
    public GameObject bottleChild;
    public GameObject shadowChild;
    private float scale;

    // Use this for initialization
    void Start () {
        bottleSpeed = Random.Range(3.0f, 4.0f);

        if (spawnLeft)
        {
            bottleSpeed *= -1;
        }
        else
        {
            
        }

        bottleChild = this.transform.Find("Bottle").gameObject;
        shadowChild = this.transform.Find("Shadow").gameObject;
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector2.right * -bottleSpeed * Time.deltaTime);

        if (bottleChild.transform.eulerAngles.z > 270)
        {
            scale = 4 - ((bottleChild.transform.eulerAngles.z - 270) / 90 * 2.5f);
        } else if (bottleChild.transform.eulerAngles.z > 180)
        {
            scale = 2 + ((bottleChild.transform.eulerAngles.z - 180) / 90 * 2.5f);
        } else if (bottleChild.transform.eulerAngles.z > 90)
        {
            scale = 4 - ((bottleChild.transform.eulerAngles.z - 90) / 90 * 2.5f);
        }
        else
        {
            scale = 2 + (bottleChild.transform.eulerAngles.z / 90 * 2.5f);
        }

        bottleChild.transform.Rotate(0, 0, bottleSpeed*2);
        shadowChild.transform.localScale = new Vector3(scale, 1, 1);

        if(transform.position.x > 8.5f || transform.position.x < -8.5f)
        {
            Destroy(this.gameObject);
        }
	}
}
