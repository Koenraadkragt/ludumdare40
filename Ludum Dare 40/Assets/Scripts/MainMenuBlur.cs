﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuBlur : MonoBehaviour {

    Material shaderMaterial;
    float blur;

    void Start()
    {
        shaderMaterial = GameObject.Find("Main Camera").GetComponent<CustomImageEffect>().EffectMaterial;
        StartCoroutine("IncreaseBlur");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            shaderMaterial.SetFloat("_Magnitude", 0);
        }
    }

    IEnumerator IncreaseBlur()
    {
        yield return new WaitForSeconds(1f);
        blur += 0.0002f;
        shaderMaterial.SetFloat("_Magnitude", blur);
        if (blur < 0.03)
        {
            StartCoroutine("IncreaseBlur");
        }
    }

    private void OnApplicationQuit()
    {
        shaderMaterial.SetFloat("_Magnitude", 0);
    }
}
