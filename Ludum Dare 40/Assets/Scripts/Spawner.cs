﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public float spawnIntervalEnemy;
    public float spawnIntervalBottles;
    public float bottleSpawnDelay;
    public GameObject[] enemies;
    public GameObject bottle;

    public void Start()
    {
        StartCoroutine("SpawnTimer", spawnIntervalEnemy);
        StartCoroutine("BottleSpawnTimer", bottleSpawnDelay);
    }

    public void Spawn()
    {
        int i = Random.Range(0, 2);
        Camera c = Camera.main;
        Vector3 spawnPosition;
        if (i == 0)
        {
            spawnPosition = new Vector3(-7.5f, Random.Range(-2.9f,-0.15f), 0);
        }
        else
        {
            spawnPosition = new Vector3(7.5f, Random.Range(-2.9f, -0.15f),0);
        }

        GameObject temp = Instantiate(enemies[Random.Range(0,enemies.Length)], spawnPosition, Quaternion.identity);
    }

    public void SpawnBottle()
    {
        int i = Random.Range(0, 2);
        bool isLeft;
        Camera c = Camera.main;
        Vector3 spawnPosition;
        if (i == 0)
        {
            spawnPosition = new Vector3(-7.5f, Random.Range(-2.9f, -0.15f), 0);
            isLeft = true;
        }
        else
        {
            spawnPosition = new Vector3(7.5f, Random.Range(-2.9f, -0.15f), 0);
            isLeft = false;
        }

        GameObject temp = Instantiate(bottle, spawnPosition, Quaternion.identity);
        temp.GetComponent<BottleThrow>().spawnLeft = isLeft;
    }


    IEnumerator SpawnTimer(float interval)
    {
        yield return new WaitForSeconds(interval);
        Spawn();
        if (interval > 1.5f)
        {
            StartCoroutine("SpawnTimer", interval * 0.95f);
        }
        else
        {
            StartCoroutine("SpawnTimer", interval);
        }
    }

    IEnumerator BottleSpawnTimer(float interval)
    {
        yield return new WaitForSeconds(interval);
        SpawnBottle();
        if (spawnIntervalBottles > 1.2f)
        {
            spawnIntervalBottles *= 0.90f;
        }
        StartCoroutine("BottleSpawnTimer", spawnIntervalBottles);
    }

}
