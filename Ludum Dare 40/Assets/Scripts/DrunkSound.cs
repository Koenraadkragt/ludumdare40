﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrunkSound : MonoBehaviour {

    private AudioSource bgMusic;
    public Material shaderMaterial;
    private float drunkPower;

	// Use this for initialization
	void Start () {
        bgMusic = this.gameObject.GetComponent<AudioSource>();
        
	}
	
	// Update is called once per frame
	void Update () {
        drunkPower = shaderMaterial.GetFloat("_Magnitude");
        bgMusic.pitch = 1 - drunkPower * Random.Range(8, 20);
        bgMusic.panStereo = (Mathf.Sin(Time.time)) * drunkPower * Random.Range(20.0f, 22.0f);
	}
}
