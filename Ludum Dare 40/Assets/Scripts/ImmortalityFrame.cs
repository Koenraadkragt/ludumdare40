﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImmortalityFrame : MonoBehaviour {

    public float immortalityTime;
    public int numberOfFlashes;
    public SpriteRenderer sprite;

    public void TriggerFlash()
    {
        GetComponentInChildren<Animator>().SetTrigger("Hurt");
        GameObject.Find("GetHitSound").GetComponent<AudioSource>().Play();
        StartCoroutine(Flash(5));
    }

    IEnumerator Flash(int currentFlash)
    {
        yield return new WaitForSeconds(immortalityTime / numberOfFlashes);
        if(currentFlash%2 == 1)
        {
            sprite.color = new Color(255, 0, 0, 0.5f);
        }
        else
        {
            sprite.color = new Color(255, 255, 255, 0.5f);
        }
        
        if(currentFlash > 0)
        {
            StartCoroutine("Flash", currentFlash - 1);
        }
        else
        {
            sprite.color = new Color(255, 255, 255, 1);

            if (GetComponent<StateMachine>().currentState != PlayerState.Dead)
            {
                GetComponent<StateMachine>().currentState = PlayerState.Idle;
            }
        }

    }

}
