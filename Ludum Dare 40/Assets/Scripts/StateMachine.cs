﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerState
{
    Idle,
    Walking,
    Attacking,
    Drinking,
    Hurt,
    Dead
}

public class StateMachine : MonoBehaviour {
    
    public PlayerState currentState;
    
}
